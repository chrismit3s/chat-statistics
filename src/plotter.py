from plotly.subplots import make_subplots
from colorsys import hsv_to_rgb
from src import OUT_DIR, moving_average
import plotly.graph_objects as go
import numpy as np


def _format_rgb(rgb):
    return "#" + "".join(f"{int(255 * x):02x}" for x in rgb)

def palette(n):
    hues = np.linspace(0, 1, n, endpoint=False)
    return [_format_rgb(hsv_to_rgb(hue, 1, 1)) for hue in hues]

def _cell_spec(n):
    return [{"colspan": n}] + [None] * int(n - 1)  # fix numpy ints

def layout(plots, row_titles, plot_titles):
    num_cols = int(np.lcm.reduce(list(map(len, plots))))

    # create specs and list of column indices for each plot
    col_indices = []
    specs = []
    for row in plots:
        colspan = num_cols // len(row)
        col_indices.append(list(range(1, num_cols + 1, colspan)))  # +1 as plotly indices start at 1 :(
        specs.append([])
        for traces in row:
            specs[-1].append({"colspan": colspan, "type": traces[0].type})
            specs[-1] += [None] * (colspan - 1)


    # create actual figure
    fig = make_subplots(
        rows=len(plots),
        cols=num_cols,
        specs=specs,
        row_titles=row_titles,
        subplot_titles=flatten(plot_titles))

    # add the traces
    for r, row in enumerate(plots):
        for n, traces in enumerate(row):
            for trace in traces:
                fig.add_trace(trace, row=r+1, col=col_indices[r][n])
    
    return fig

def flatten(l):
    if not isinstance(l, list):
        return [l]

    res = []
    for x in l:
        res += flatten(x)
    return res


class Plotter():
    def __init__(self, legenditems):
        self.palette = dict(zip(legenditems, palette(len(legenditems))))
        self.legenditems = set()  # a set for what items are on the legend already
        self.plots = []  # a list of lists (rows) of lists (subplots) of traces
        self.row_titles = []
        self.plot_titles = []

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.show()

    def show(self):
        fig = layout(self.plots,
                     self.row_titles,
                     self.plot_titles)

        fig.show(renderer="browser")

    def add_row(self, title):
        self.plots.append([])
        self.plot_titles.append([])
        self.row_titles.append(title)

    def add_plot(self, title):
        self.plots[-1].append([])
        self.plot_titles[-1].append(title)

    def add_line(self, x, y, name=None, smooth=1, showghost=False, color=None):
        color = color or self.palette.get(name, "#000000")

        if name is not None:
            showlegend = name not in self.legenditems
            self.legenditems.add(name)
        else:
            showlegend = False

        if smooth > 1 and showghost:
            self.plots[-1][-1].append(
                go.Scatter(
                    x=x,
                    y=moving_average(y, n=smooth),
                    name=name,
                    legendgroup=name,
                    mode="lines",
                    marker_color=color,
                    opacity=1,
                    showlegend=showlegend))
            self.plots[-1][-1].append(
                go.Scatter(
                    x=x,
                    y=y,
                    name=name,
                    legendgroup=name,
                    mode="lines",
                    marker_color=color,
                    opacity=0.2,
                    hoverinfo="skip",
                    showlegend=False))
        else:
            self.plots[-1][-1].append(
                go.Scatter(
                    x=x,
                    y=moving_average(y, n=smooth),
                    name=name,
                    legendgroup=name,
                    mode="lines",
                    marker_color=color,
                    opacity=1,
                    showlegend=showlegend))

    def add_area(self, x, y, name=None, smooth=1, color=None):
        color = color or self.palette.get(name, "#000000")

        if name is not None:
            showlegend = name not in self.legenditems
            self.legenditems.add(name)
        else:
            showlegend = False

        self.plots[-1][-1].append(
            go.Scatter(
                x=x,
                y=moving_average(y, n=smooth),
                name=name,
                legendgroup=name,
                stackgroup="stack",
                mode="lines",
                hoverinfo="x+y",
                line_width=0.5,
                line_color=color,
                marker_color=color,
                opacity=1,
                showlegend=showlegend))

    def add_pie(self, x, y):
        colors = [self.palette.get(name, "#000000") for name in x]

        self.plots[-1][-1].append(
            go.Pie(
                labels=x,
                values=y,
                opacity=0.6,
                marker_colors=colors,
                marker_line_color=colors,
                marker_line_width=0.5,
                showlegend=False))
