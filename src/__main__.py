from collections import defaultdict
from os.path import exists
from src import Chat, Plotter, asciify, moving_average
from sys import argv
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots


if __name__ == "__main__":
    # generate chat object from path
    if len(argv) != 2:
        raise FileNotFoundError("No path passed")
    if not exists(argv[1]):
        raise FileNotFoundError(f"File {path} not found")

    chat = Chat(argv[1])
    date_range = chat.date_range
    plotter = Plotter(chat.participants)
    smooth_step = 20

    plotter.add_row("Daily")
    plotter.add_plot("Daily messages")
    for p in chat.participants:
        plotter.add_line(
            x=date_range,
            y=chat.message_count_per_day(p),
            name=p,
            smooth=smooth_step,
            showghost=True)
    plotter.add_line(
        x=date_range,
        y=chat.message_count_per_day(),
        name="Total",
        color="#000000",
        smooth=smooth_step,
        showghost=True)

    plotter.add_plot("Daily words")
    for p in chat.participants:
        plotter.add_line(
            x=date_range,
            y=chat.word_count_per_day(p),
            name=p,
            smooth=smooth_step,
            showghost=True)
    plotter.add_line(
        x=date_range,
        y=chat.word_count_per_day(),
        name="Total",
        color="#000000",
        smooth=smooth_step,
        showghost=True)

    plotter.add_row("Ratios")
    plotter.add_plot("Words per message")
    for p in chat.participants:
        plotter.add_line(
            x=np.array([msg.time for msg in chat.messages_sent_by(p)]),
            y=np.array([msg.word_count() for msg in chat.messages_sent_by(p)]),
            name=p,
            smooth=smooth_step,
            showghost=False)
    plotter.add_line(
        x=np.array([msg.time for msg in chat.messages_sent_by()]),
        y=np.array([msg.word_count() for msg in chat.messages_sent_by()]),
        name="Total",
        smooth=smooth_step*40,  # needs *a lot* more smoothing
        showghost=False)

    plotter.add_plot("Daily speakshare")
    total = chat.word_count_per_day()
    for p in chat.participants:
        plotter.add_area(
            x=date_range,
            y=np.divide(
                chat.word_count_per_day(p),
                total.astype("float"),
                out=np.full(total.shape, 1 / len(chat.participants), dtype="float"),
                where=(total != 0)),
            name=p,
            smooth=smooth_step)
    plotter.add_line(
        x=date_range,
        y=[0.5] * len(date_range),
        color="#000000")

    plotter.add_plot("Total speakshare")
    plotter.add_pie(
        x=chat.participants,
        y=[sum(chat.word_count_per_day(p)) for p in chat.participants])

    plotter.show()
