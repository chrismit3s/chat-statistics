from os.path import join


OUT_DIR = join(".", "out", "")

MEDIA_OMMITED = "<Media omitted>"
MESSAGE_DELETED = "This message was deleted"
FILE_ATTACHED = "(file attached)"


from src.helpers import asciify, moving_average
from src.message import Message, MessageType
from src.plotter import Plotter
from src.chat import Chat
