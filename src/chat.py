from datetime import timedelta
#from functools import cached_property  # doesnt work wtf https://docs.python.org/3/library/functools.html#functools.cached_property
from src import Message
import numpy as np


class Chat():
    def __init__(self, path):
        self.participants = []
        self._dict = None

        with open(path, encoding="utf8") as file:
            prev_line = ""
            self.messages = []
            for line in file:
                has_date_header = bool(Message.meta_pattern.match(line))

                # a date header was found, might be a messages or some admin stuff
                if has_date_header and len(prev_line) > 0:
                    # parse and clear out previous message
                    message = Message(prev_line)
                    self.messages.append(message)
                    self.add_participant(message.sender)
                    prev_line = ""

                # new message or no date header
                if Message.message_pattern.match(line) or not has_date_header:
                    prev_line += line

            # dont forget the last (unparsed) message
            self.messages.append(Message(prev_line))

    #@cached_property
    def dict(self):
        return {"time":    [message.time    for message in self.messages],
                "sender":  [message.sender  for message in self.messages],
                "content": [message.content for message in self.messages],
                "type":    [message.type    for message in self.messages]}

    @property
    def date_range(self):
        return np.arange(self.messages[0].time, self.messages[-1].time + timedelta(days=1), dtype="datetime64[D]")

    def add_participant(self, participant):
        if participant not in self.participants:
            self.participants.append(participant)

    def is_group_chat(self):
        return len(self.participants) > 2

    def messages_sent_by(self, participants=None):
        if participants is None:
            participants = self.participants

        # if only a single participant or a list/tuple/etc is passed, turn it into a set
        if type(participants) is str:
            participants = {participants}
        elif type(participants) is not set:
            participants = set(participants)

        # return generator for every message set by one of participants
        return (message
                for message
                in self.messages
                if message.sender in participants)

    def message_count_per_day(self, participant=None):
        dates = np.array([np.datetime64(message.time, "D") for message in self.messages_sent_by(participant)], dtype="datetime64[D]") - np.datetime64(self.messages[0].time, "D")
        bins = np.bincount(dates.astype("int"))
        bins.resize(self.date_range.shape)
        return bins

    def word_count_per_day(self, participant=None):
        first = np.datetime64(self.messages[0].time, "D")
        last = np.datetime64(self.messages[-1].time, "D")
        bins = np.zeros(shape=(last - first).astype("int") + 1,
                        dtype="int")

        for msg in self.messages_sent_by(participant):
            bins[(np.datetime64(msg.time, "D") - first).astype("int")] += msg.word_count()

        bins.resize(self.date_range.shape)
        return bins
