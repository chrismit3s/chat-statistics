from datetime import datetime
from enum import Enum, auto, unique
from src import MEDIA_OMMITED, MESSAGE_DELETED, FILE_ATTACHED, asciify
import re


class MessageType(Enum):
    MEDIA = auto()
    DELETED = auto()
    TEXT = auto()


class Message():
    # group 1 is date and time, group 2 is sender, and group 3 is content
    # [       1       ]   [  2  ]  [   3   ]
    # 06/09/1337, 04:20 - Someone: Something
    message_pattern = re.compile(r"^(\d{2}/\d{2}/\d{4}, \d{2}:\d{2}) - ([^:]*): (.*)")
    # meta message is for all other non user message things which appear in the logs
    # can also be used to check if the previous message ended
    # 06/09/1337, 04:20 - Someone added SomeoneElse to the group
    # 04/02/9001, 20:49 - Messages to this group are now secured with end-to-end encryption. Tap for more info.
    meta_pattern = re.compile(r"^(\d{2}/\d{2}/\d{4}, \d{2}:\d{2}) - (.*)")

    def __init__(self, message_str):
        # parse
        match = Message.message_pattern.match(message_str)
        if match is None:
            raise ValueError(f"Message text does not fit message pattern (message_str = {message_str!r})")

        # extract
        self.content = match.group(3)
        self.sender = match.group(2)
        self.time = datetime.strptime(match.group(1), "%d/%m/%Y, %H:%M")

        # filter special messages
        if self.content == MEDIA_OMMITED or self.content.endswith(FILE_ATTACHED):
            self.type = MessageType.MEDIA
            self.content = ""
        elif self.content == MESSAGE_DELETED:
            self.type = MessageType.DELETED
            self.content = ""
        else:
            self.type = MessageType.TEXT

    def __str__(self):
        return f"[{self.time.isoformat()} - {self.sender.ljust(8, ' ')[:8]}] {self.content[:20]}"

    def __repr__(self):
        return f"[{self.time.isoformat()} - {self.ascii_sender[:10]:<10}] {self.ascii_content[:30]}"

    @property
    def ascii_content(self):
        return asciify(self.content)

    @property
    def ascii_sender(self):
        return asciify(self.sender)

    def word_count(self):
        return len(self.ascii_content.split())
