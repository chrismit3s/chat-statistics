from string import printable
import numpy as np


# from https://stackoverflow.com/q/14313510
def moving_average(a, n=2):
    ret = np.cumsum(a, dtype="float")
    ret[n:] = ret[n:] - ret[:-n]
    ret[n:] /= n
    ret[:n] /= np.arange(n) + 1
    return ret


def asciify(s):
    """
    removes all non printable and non ascii characters and removes
    unnecessary whitespace, string returned is in lowercase
    """
    # remove non printables and non ascii
    chars = set(printable)
    s = "".join(filter(lambda x: x in chars, s.lower()))

    # remove double whitespace
    s = " ".join(s.split())

    return s
